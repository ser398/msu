#include <stdio.h>
#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <fstream>
#include <assert.h>
#include <sstream>
#define S_journal "20"
#define S_autor "32"
#define S_article "10"
using namespace std;
struct element{
public:
	string journal;
	int year, number;
	string autor, article;

	element() :journal(), year(), number(), autor(), article(){};
	element(string j, int y, int n, string au, string article) :
		journal(j), year(y), number(n), autor(au), article(article){};

	void set_journal(string journall){ journal = journall; };
};
struct writter{
	FILE* f;
	vector<element> v;
	writter(FILE *g) :f(g){};
	void add(element a){
		v.push_back(a);
	}
	void print(){
		printf("% " S_journal "s year num % " S_autor "s % " S_article "s", "journal",
			"autor", "article");
		for (int i = 0; i < v.size(); i++){
			printf("% " S_journal "s  %04d %02d % " S_autor "s % " S_article "s\n",
				v[i].journal.c_str(), v[i].year, v[i].number,
				v[i].autor.c_str(), v[i].article.c_str());
		}
		v.clear();
	}
};
struct base{
private:
	enum{
		_default, _journal, _article,
	};

	vector<element> list;
	template<int type>
	struct cmp{
		bool operator ()(const element &a, const element &b) const {
			if (type == _journal)return a.journal < b.journal;
			if (type == _article)return a.article < b.article;

			if (type == _default){
				if (a.journal != b.journal)return a.journal < b.journal;
				if (a.year != b.year)return a.year < b.year;
				if (a.number != b.number)return a.number < b.number;
				if (a.autor != b.autor)return a.autor < b.autor;
				return a.article < b.article;
			}
		};

	};
	writter wrt;
	multiset<element, cmp<_default> > dft;
	multiset<element, cmp<_journal> > jrn;
	multiset<element, cmp<_article> > art;
public:
	base() :wrt(stdout){
	};
	base(base &ot) :wrt(stdout), dft(ot.dft), art(ot.art), jrn(ot.jrn){
	}
	void add(const element& t){
		art.insert(t);
		jrn.insert(t);
		dft.insert(t);
	}
	void select(string journal){
		element tmp;
		//cerr << journal << endl;
		//tmp.set_journal(journal);
		for (auto it = dft.begin(); it != dft.end(); it++){
			//cerr << "!";
			if (it->journal == journal)
			wrt.add(*it);
		}
		wrt.print();
	}

};
class pom{
public:
	base b;
	pom(){}
	pom(base bb) :b(bb){

	}
	void add(const element& a){
		b.add(a);
	}
	bool get(){
		string s;
		if (!getline(cin, s))
			return false;
		stringstream sin(s);
		string slt, j;
		while (sin >> slt >> j){
			cerr << j << endl;
			b.select(j);
		}

	}
};
int main(){
	ifstream finn("ololo.txt", std::ifstream::in);
	base b;
	pom qqq(b);

	string j, au, ar;
	int y, n;
	while (finn >> j >> y >> n >> au >> ar){
		qqq.b.add(element(j, y, n, au, ar));
	}
	qqq.get();
	return 0;
}
